import requests,json,os,datetime,time
from diff import diff_assessment



token = os.environ["PLATFORM_TOKEN"]
if "SLACK_WEBHOOK" in os.environ:
    slack_webhook = os.environ["SLACK_WEBHOOK"]
if "TEAMS_WEBHOOK" in os.environ:
    teams_webhook = os.environ["TEAMS_WEBHOOK"]
notify_on_success = os.environ["NOTIFY_SUCCESS"].lower()
notify_on_error = os.environ["NOTIFY_ERROR"].lower()
notify_threshold = os.environ["NOTIFY_THRESHOLD"].lower()
monitor_frequency = os.environ["MONITOR_FREQUENCY"]


def get_all_new_assessments(processed_assessments):
    assessments_to_process = []
    query = """query{
                    auto{
                        assessments{
                            ref
                            analysis{
                                status
                            }
                        }
                    }
                } """
    headers = {"Authorization": "Bearer " + token,'Accept-Encoding': 'gzip, deflate, br', 'Accept': 'application/json'}
    r1 = requests.post(url='https://lab-api.nowsecure.com/graphql', headers=headers,json={"query":query})        
    all_assessments = json.loads(r1.text)
    for assessment in all_assessments['data']['auto']['assessments']:
        if assessment['ref'] in processed_assessments:
            continue
        else:
            assessments_to_process.append([assessment['ref'],assessment['analysis']['status']])
    return assessments_to_process

def process_assessment(ref):
    crit_vulns = 0
    high_vulns = 0
    med_vulns = 0
    low_vulns = 0
    high_crit_list = []
    query = """query{
                    auto{
                        assessment(ref:\"""" + ref +  """\"){
                            platformType
                            packageKey
                            applicationRef
                            title
                            taskId
                            report{
                                findings{
                                    title
                                    affected
                                    cvss
                                }
                            }
                        }
                    }
                }  """
    time.sleep(60)
    headers = {"Authorization": "Bearer " + token,'Accept-Encoding': 'gzip, deflate, br', 'Accept': 'application/json'}
    r1 = requests.post(url='https://lab-api.nowsecure.com/graphql', headers=headers,json={"query":query})        
    report_findings = json.loads(r1.text)
    for finding in report_findings['data']['auto']['assessment']['report']['findings']:
        if finding['affected'] == True:
            if finding['cvss'] >=9 :
                crit_vulns +=1
                high_crit_list.append(finding['title'])
            if finding['cvss'] >=7 and finding['cvss'] <9 :
                high_vulns +=1
                high_crit_list.append(finding['title'])
            if finding['cvss'] >=3 and finding['cvss'] < 7 :
                med_vulns +=1
            if finding['cvss'] <3 :
                low_vulns +=1
    issue_count = {
        "app_name": report_findings['data']['auto']['assessment']['title'],
        "app_platform": report_findings['data']['auto']['assessment']['platformType'],
        "app_package": report_findings['data']['auto']['assessment']['packageKey'],
        "task_id": report_findings['data']['auto']['assessment']['taskId'],
        "app_ref": report_findings['data']['auto']['assessment']['applicationRef'],
        "low": low_vulns,
        "med": med_vulns,
        "high": high_vulns,
        "crit": crit_vulns,
        "crit_list": high_crit_list,
        "report_ref": ref
    }

    return issue_count
    
def get_processed_assessments():
    with open("assessment_list.txt","r") as assessment_file:
        processed_assessments = assessment_file.readlines()
        processed_assessments = [x.strip() for x in processed_assessments]
    return processed_assessments

def write_new_processed_list(new_list):
    os.remove("assessment_list.txt")
    with open("assessment_list.txt", "w+") as output_file:
        output_file.writelines("%s\n" % ref for ref in new_list)

def send_failed_message(ref):
    query = """query{
                    auto{
                        assessment(ref:\"""" + ref +  """\"){
                            platformType
                            title
                            taskId
                            
                        }
                    }
                } """  
    headers = {"Authorization": "Bearer " + token,'Accept-Encoding': 'gzip, deflate, br', 'Accept': 'application/json'}
    r1 = requests.post(url='https://lab-api.nowsecure.com/graphql', headers=headers,json={"query":query})        
    app_info = json.loads(r1.text)
    if r1.status_code > 201:
        print("Error! " + r1.message)
        return False
    if "SLACK_WEBHOOK" in os.environ:
        slack_data = {
            "text": "Application: " + app_info['data']['auto']['assessment']['title'] + " for " + app_info['data']['auto']['assessment']['platformType'] + " has failed to complete successfully, you may want to investigate. Please reference task id " + str(app_info['data']['auto']['assessment']['taskId']) 
        }
        r4 = requests.post(slack_webhook, json=slack_data) 
        if r4.status_code > 201:
            print ("Slack webhook error " + r4.message)
            return False
    if "TEAMS_WEBHOOK" in os.environ:
        teams_data = {
            "@type": "MessageCard",
            "@context": "http://schema.org/extensions",
            "summary": "NS Platform Alerts",
            "themeColor": "#ff0000",
            "title": "NowSecure Platform Alerts",
            "sections": [
                {
                    "text": "Application: " + app_info['data']['auto']['assessment']['title'] + " for " + app_info['data']['auto']['assessment']['platformType'] + " has failed to complete successfully, you may want to investigate. Please reference task id " + str(app_info['data']['auto']['assessment']['taskId'])
                }
            ]
        }
        r3 = requests.post(teams_webhook, json=teams_data) 
        if r3.status_code > 201:
            print ("Teams webhook error " + r3.content)
            return False
    return True


def send_success_message(assessment_info):
    new_findings, remediated_findings = [],[]
    diff_report = diff_assessment(assessment_info['app_ref'], assessment_info['report_ref'],token)
    if diff_report == False:
        new_findings == False
    else:
        new_findings = diff_report['new_findings']
        remediated_findings = diff_report['remediated_findings']
    if new_findings == False:
        new_findings = "Not enough information to diff report"
        remediated_findings = "Not enough information to diff report"
    elif new_findings == []:
        new_findings = "No new findings for this report"
    if remediated_findings == []:
        remediated_findings = "No findings remediated"
    if assessment_info['crit_list'] == []:
        assessment_info['crit_list'] = "No critical or high risks detected in this assessment"
    color = "#0388fc"
    if assessment_info['low'] > 0:
        color = "#008080"
    if assessment_info['med'] > 0:
        color = "#FFC300"
    if assessment_info['high'] > 0:
        color = "#FF0000"
    if assessment_info['crit'] > 0:
        color = "#8c0101"
    if "TEAMS_WEBHOOK" in os.environ:
        teams_data = {
            "@type": "MessageCard",
            "@context": "http://schema.org/extensions",
            "summary": "NS Platform Alerts",
            "themeColor": color,
            "title": "NowSecure Platform Alerts",
            "sections": [
                {
                    "text": "<h2>The " + assessment_info['app_platform'] + " app " + assessment_info['app_name'] + " (package/bundle id: " + assessment_info['app_package'] + ") has just completed an assessment on the NowSecure Platform.</h2>"
                },
                {
                    "text": "<ul><li>Critical Vulns: " + str(assessment_info['crit']) + "</li><li>High Vulns:  " + str(assessment_info['high']) + "</li><li>Medium Vulns:  " + str(assessment_info['med']) + "</li><li>Low Vulns:  " + str(assessment_info['low']) + "</li></ul>"
                },
                {
                    "text": "Critical and high risk vulns: " + str(assessment_info['crit_list'])
                },
                {
                    "text": "<a href=\"https://lab.nowsecure.com/app/" + assessment_info['app_ref'] + "/assessment/" + str(assessment_info['task_id']) + "\">Go see the report in NowSecure Platform</a>"
                },
                {
                    "text": "Differential Report <br> New Findings: " + str(new_findings) + "<br>Remediated findings: " + str(remediated_findings)
                }
            ]
        }
        r3 = requests.post(teams_webhook, json=teams_data) 
        if r3.status_code != 200:
            print ("Teams webhook error " + r3.content)

    if "SLACK_WEBHOOK" in os.environ:
        weburl = "https://lab.nowsecure.com/app/" + assessment_info['app_ref'] + "/assessment/" + str(assessment_info['task_id'])
        slack_data = {
            "text": "The " + assessment_info['app_platform'] + " app " + assessment_info['app_name'] + " (package/bundle id: " + assessment_info['app_package'] + ") has just completed an assessment on the NowSecure Platform.",
           

            "attachments": [
                {
                    "fallback": "NowSecure Platform",
                    "title": "Report Summary",
                    "color": color,
                    "text": "The following issues were found:",
                    "fields": [
                        {
                            "value": str(assessment_info['crit']) + " critical risk",
                            "short": "true"
                        },
                        {
                            "value": str(assessment_info['high']) + " high risk",
                            "short": "true"
                        },
                        {
                            "value": str(assessment_info['med']) + " medium risk",
                            "short": "true"
                        },
                        {
                            "value": str(assessment_info['low']) + " low risk",
                            "short": "true"
                        },
                        {
                            "value": "High or critical risk items found: " + str(assessment_info['crit_list'])
                        },
                        {
                            "value": "You can view the assessment at: " + weburl
                        },
                        {
                            "value":" Differential Report \n New Findings: " + str(new_findings) + "\nRemediated findings: " + str(remediated_findings)
                        }
                    ],
                    "footer": "Thank you for using NowSecure Platform!"
                },
            ]
        }
        # slack_header = 'Content-type: application/json'
        r4 = requests.post(slack_webhook, json=slack_data) 
        if r4.status_code != 200:
            print ("Slack webhook error " + r4.content)

with open("assessment_list.txt",'w+') as first_run_file:
    processed = []
    all_assessments = get_all_new_assessments(processed)
    for ref in all_assessments:
        first_run_file.write(ref[0] +"\n")
while True:
    processed_assessments = get_processed_assessments()
    new_assessments = get_all_new_assessments(processed_assessments)

    for report in new_assessments:
        if report[1] == 'cancelled':
            processed_assessments.append(report[0])
        if report[1] in ['processing','pending']:
            print("Found new report " + str(report[0]) + " still in progress, will process when complete")
            continue
        if report[1] == None:
            continue
        if report[1] == 'failed':
            if notify_on_error == 'true':
                success = send_failed_message(report[0])
                if success:
                    processed_assessments.append(report[0])
            else:
                processed_assessments.append(report[0])
        if report[1] == 'completed':
            if(notify_on_success == 'true'):
                print("processing successful report")
                assessment_info = process_assessment(report[0])
                if((notify_threshold == "none") or (notify_threshold =='low') or (notify_threshold == "medium" and (assessment_info["med"] > 0 or assessment_info["high"] > 0 or assessment_info["crit"]) > 0) or (notify_threshold == "high" and (assessment_info["high"] > 0 or assessment_info["crit"]) > 0) or (notify_threshold == "critical" and assessment_info["crit"] > 0)):
                    print("report met threshold, sending message to slack!")
                    send_success_message(assessment_info)
                else:
                    print("report did not meet threshold, moving on")
            processed_assessments.append(report[0])

    write_new_processed_list(processed_assessments)
    print("sleeping")
    time.sleep(int(monitor_frequency))