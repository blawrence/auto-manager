import requests, json

def graph_query(query,token):
    headers = {"Authorization": "Bearer " + token,'Accept-Encoding': 'gzip, deflate, br', 'Accept': 'application/json'}
    r1 = requests.post(url='https://lab-api.nowsecure.com/graphql', headers=headers,json={"query":query})        
    info = json.loads(r1.text)
    return info