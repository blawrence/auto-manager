FROM python:3.8-slim-buster

LABEL maintainer blawrence

RUN pip3 install --upgrade pip requests 

ADD main.py diff.py graph.py /

CMD ["python3", "-u", "main.py"]