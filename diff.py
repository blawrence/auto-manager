from graph import graph_query
import json, requests

test_token = "eyJhbGciOiJFUzUxMiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI5NmNkMWM5MC1iMzAxLTRhMmYtYmFkMy1iYmY5Yjk3ZjY1ZTciLCJpc3MiOiJsYWItYXBpLm5vd3NlY3VyZS5jb20iLCJzdWIiOiIzNWVmMzJmMi1iNTJjLTQ0ODUtOWNlZi02OWJlYzBhZGJjMWIiLCJuYW1lIjoidGVzdCIsImlhdCI6MTUzODU5MzMzMCwiYXVkIjoibGFiLWF1dG9tYXRlZCJ9.AWnbXlEylEDV8T04C3SW-SHRUBSQ_IT2WbSUv1vID_Yh_rcmeBkAYoRkugqmuWj6sXwkL_iQVezrqCZGEA-CDttaAdqiR6p5tfLB9vV0mn38CJ03oJOzWrMuDZyJAWwLNPaTpu2XlDPdBV5NtLSlgSWhC_-6hwqm74TLThaops3MhZW9"
test_ref = 'a8518c54-cdbb-11ea-90f0-ebd2aac2f30b'
test_app_ref = '17d3a066-ff2e-11e8-92cd-e3681004a575'


def scrub_old_findings(old_findings):
    affected_findings = []
    for report in old_findings['data']['auto']['assessments']:
        for findings in report['report']['findings']:
            if findings['affected'] == True and findings['cvss'] > 0 and [findings['checkId'],findings['title'],findings['severity']] not in affected_findings:
                affected_findings.append([findings['checkId'],findings['title'],findings['severity']])
    return affected_findings

def diff_assessment(app_ref,report_ref, token):
    query = """query{
                    auto{
                        application(ref:\"""" + app_ref + """\"){
                            assessments{
                                ref
                                createdAt
                                analysisConfigLevel
                                analysis{
                                    status
                                }
                            }
                        }
                    }
                }"""
    report_list = graph_query(query,token)
    new_report_index = 0
    reports_to_find = 0
    for report in report_list['data']['auto']['application']['assessments']:
        if report['ref'] == report_ref:
            config_level = report['analysisConfigLevel']
            break
        new_report_index += 1
    if new_report_index == 0:
        return False
    loop_index = new_report_index - 1
    if config_level in ['BASELINE', None]:
        reports_to_find = 3
    elif config_level == 'ADVANCED':
        reports_to_find = 1
    else:
        return False
    if reports_to_find > loop_index:
        return False
    compare_list= []
    while loop_index >=0 and reports_to_find >0:
        if loop_index == 0 and reports_to_find >0:
            return False
        if report_list['data']['auto']['application']['assessments'][loop_index]['analysis']['status'] != 'completed':
            loop_index -=1
            continue
        if config_level == report_list['data']['auto']['application']['assessments'][loop_index]['analysisConfigLevel']:
            compare_list.append("\"" + report_list['data']['auto']['application']['assessments'][loop_index]['ref'] + "\"")
            reports_to_find -= 1
        loop_index -= 1
    
    old_reports_query = """query{
                                auto{
                                    assessments(refs:""" + str(compare_list).replace("\'", "") + """){
                                        report{
                                            findings{
                                                checkId
                                                title
                                                affected
                                                cvss
                                                severity
                                            }
                                        }
                                    }
                                }
                            }"""
    # stub for regression identification
    # all_reports_query = 
    compare_report_query = """query{
                                auto{
                                    assessment(ref:\"""" + report_ref + """\"){
                                        report{
                                            findings{
                                                checkId
                                                title
                                                affected
                                                cvss
                                                severity
                                            }
                                        }
                                    }
                                }
                            }"""
    old_findings = scrub_old_findings(graph_query(old_reports_query,token))
    compare_findings = graph_query(compare_report_query,token)
    new_findings = []
    remediated_findings = []
    current_findings = []
    for finding in compare_findings['data']['auto']['assessment']['report']['findings']:
        current_findings.append([finding['checkId'],finding['title'],finding['severity']])
        if finding['affected'] =='false' or not finding['cvss'] > 0:
            continue
        if [finding['checkId'],finding['title'],finding['severity']] in old_findings:
            continue
        else:
            new_findings.append([finding['title'],finding['severity']])
    for finding in old_findings:
        if finding not in current_findings:
            remediated_findings.append(finding)
    return {
        "new_findings":new_findings,
        "remediated_findings":remediated_findings
    }

#diff_assessment(test_app_ref,test_ref,test_token)
#print('buttes')






            


