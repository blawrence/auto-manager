# NowSecure Chat Integration

This is a quick Docker container to monitor an account in NowSecure Platform. It can push notifications based on set criteria to Slack and/or MS Teams. It also performs a diff of the report that it is reporting on to provide information on new and remediated bugs. 

- Assumes docker is being used, but could be standalone as needed

Looks for the SLACK_WEBHOOK or TEAMS_WEBHOOK to determine where to send notifications. Can do both if desired. 


## Using docker to run 

1. clone the repository
2. open a terminal in the directory
3. Run `docker build . -t <NAME YOUR CONTAINER>`

Sample docker initialization:

```shell
docker run \
 -e PLATFORM_TOKEN=<YOUR KEY GOES HERE> \
 -e SLACK_WEBHOOK=<YOUR SLACK WEBHOOK GOES HERE> \
 -e MONITOR_FREQUENCY=<DELAY IN SECONDS BETWEEN CHECKS OF THE PLATFORM- THINK ~600>
 -e TEAMS_WEBHOOK=<YOUR TEAMS WEBHOOK GOES HERE> \
 -e NOTIFY_SUCCESS=<`True` or `False` DO YOU WANT NOTIFICATIONS FOR APPS THAT COMPLETE SUCCESSFULLY> \
 -e NOTIFY_ERROR=<`True` or `False` DO YOU WANT NOTIFICATIONS IF AN APP DOES NOT COMPLETE AN ASSESSMENT> \
 -e NOTIFY_THRESHOLD=<'medium' 'high' 'critical' or 'none' THE BAR YOU WANT TO SET FOR SENDING NOTIFICATIONS OF NEW ASSESSMENTS> \
 --name <NAME THE MANAGEMENT CONTAINER SOMETHING WITTY AND POSSIBLY SARCASTIC> \
 -d \
<name of docker container>
```
